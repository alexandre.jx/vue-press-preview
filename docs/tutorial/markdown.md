---
lang: en-US
title: Markdown
description: Testing markdown capabilities
---

# Markdown

this is testing the frontmatter settings.

| Command | Description |
| --- | --- |
| git status | List all new or modified files |
| git diff | Show file differences that haven't been staged |

[Home](../README.md)  


```c#

public class Test() {
    var test = "this is a string!";
}

```

```js

function test() {
    test = "12";
}

```

