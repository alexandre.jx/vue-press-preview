module.exports = {
    lang: 'en-US',
    title: 'Hello, VuePress!',
    description: 'This is my first VuePress site',
    base: process.env.VUEPRESS_BASE || '/',
    plugins: [
          '@vuepress/plugin-search'
        ],
    themeConfig: {
      logo: 'https://vuejs.org/images/logo.png',
      sidebar: {
        '/': [{
            text: "Guide",
            children: [
                "/",
                "CONTRIBUTING.md"
            ]
        }],
        '/tutorial/': [{
            text: "Tutorial",
            children: [
                '/tutorial/frontmatter.md',
                '/tutorial/markdown.md'
            ]
        }],
    },
      navbar: [
        {
            text: "tutorial",
            link: "/tutorial",
            children: [
                '/tutorial/frontmatter.md',
                '/tutorial/markdown.md'
            ]
        }
      ],
    },  
  }